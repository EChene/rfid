-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `nfcpresent`
--
DROP DATABASE `nfcpresent`;
CREATE DATABASE IF NOT EXISTS `nfcpresent` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nfcpresent`;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `a_p_id` int(6) NOT NULL,
  `a_c_id` int(6) NOT NULL,
  `a_note` int(2) NOT NULL,
  `a_descri` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `avis`
--

INSERT INTO `avis` (`a_p_id`, `a_c_id`, `a_note`, `a_descri`) VALUES
(1, 1, 6, 'Traces d\'usures');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `c_id` int(6) NOT NULL,
  `c_nom` varchar(20) NOT NULL,
  `c_prenom` varchar(20) NOT NULL,
  `c_sexe` tinyint(1) NOT NULL,
  `c_ville` varchar(20) NOT NULL,
  `c_adresse` varchar(50) NOT NULL,
  `c_dateNaiss` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`c_id`, `c_nom`, `c_prenom`, `c_sexe`, `c_ville`, `c_adresse`, `c_dateNaiss`) VALUES
(1, 'Chene', 'Emmanuel', 1, 'Bordeaux', 'Adresse 1', '2016-11-09'),
(2, 'Adlal', 'Medhi', 1, 'Antibes', 'Adresse 2', '2017-05-02');

-- --------------------------------------------------------

--
-- Structure de la table `magasin`
--

CREATE TABLE `magasin` (
  `m_id` int(6) NOT NULL,
  `m_nom` varchar(50) NOT NULL,
  `m_adresse` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `magasin`
--

INSERT INTO `magasin` (`m_id`, `m_nom`, `m_adresse`) VALUES
(1, 'Carrefour Antibes', 'Antibes'),
(2, 'Lidle Antibes', 'Antibes'),
(3, 'Leclerc Antibes', 'Antibes'),
(4, 'Lidle Valbonne', 'Valbonne'),
(5, 'Carrefour Valbonne', 'Valbonne'),
(6, 'Leclerc Valbonne', 'Valbonne');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `p_id` int(6) NOT NULL,
  `p_nom` varchar(50) NOT NULL,
  `p_descri` varchar(255) NOT NULL,
  `p_prix` int(6) NOT NULL,
  `p_detail` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`p_id`, `p_nom`, `p_descri`, `p_prix`, `p_detail`) VALUES
(1, 'Place de concert', 'Place pour un concert ', 50, 'L’événement se déroulera ...');

-- --------------------------------------------------------

--
-- Structure de la table `profite`
--

CREATE TABLE `profite` (
  `pr_p_id` int(6) NOT NULL,
  `pr_pro_id` int(6) NOT NULL,
  `pr_debut` date NOT NULL,
  `pr_fin` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `profite`
--

INSERT INTO `profite` (`pr_p_id`, `pr_pro_id`, `pr_debut`, `pr_fin`) VALUES
(1, 1, '2017-05-01', '2017-05-16'),
(2, 2, '2017-05-02', '2017-05-10');

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE `promotion` (
  `p_id` int(6) NOT NULL,
  `p_reduction` int(2) NOT NULL,
  `p_descri` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `promotion`
--

INSERT INTO `promotion` (`p_id`, `p_reduction`, `p_descri`) VALUES
(1, 50, 'Réduction 50%'),
(2, 20, 'Réduction 20%');

-- --------------------------------------------------------

--
-- Structure de la table `situer`
--

CREATE TABLE `situer` (
  `m_id` int(6) NOT NULL,
  `p_id` int(6) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `situer`
--

INSERT INTO `situer` (`m_id`, `p_id`, `quantite`) VALUES
(1, 1, 100),
(2, 2, 10);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`a_p_id`,`a_c_id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`c_id`);

--
-- Index pour la table `magasin`
--
ALTER TABLE `magasin`
  ADD PRIMARY KEY (`m_id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`p_id`);

--
-- Index pour la table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`p_id`);

--
-- Index pour la table `situer`
--
ALTER TABLE `situer`
  ADD PRIMARY KEY (`m_id`,`p_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `c_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `magasin`
--
ALTER TABLE `magasin`
  MODIFY `m_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `p_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `p_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
