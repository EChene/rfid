package src.main.java.fr.unice.iut.nfc.resources;

public class Produit {

    private int id;
    private String nom;
    private String description;
    private double prix;
    private String detail;

    public Produit(int unId, String unNom, String uneDescription, double unPrix, String unDetail) {

        this.id = unId;
        this.nom = unNom;
        this.description = uneDescription;
        this.prix = unPrix;
        this.detail = unDetail;

    }

    public Produit() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
