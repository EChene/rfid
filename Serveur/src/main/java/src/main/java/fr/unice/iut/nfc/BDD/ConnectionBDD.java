package src.main.java.fr.unice.iut.nfc.BDD;


import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnknownWebAppException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBDD {

    private ConnectionBDD(){

    }

    public static Connection getConnection(String url) {
        Connection connection;
        try {
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver);
            String user = "root";
            String pwd = "";

            connection = DriverManager.getConnection(url, user, pwd);

        } catch (SQLException e) {
            throw new UnknownWebAppException("Impossible to get connection", e);
        } catch (ClassNotFoundException e) {
            throw new UnknownWebAppException("Class not found ", e);
        }
        return connection;
    }

}
