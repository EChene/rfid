package src.main.java.fr.unice.iut.nfc.services;

import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnauthorizedWebAppExeption;
import src.main.java.fr.unice.iut.nfc.providers.ProduitsProvider;
import src.main.java.fr.unice.iut.nfc.resources.Produit;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/produit")
public class ProduitImpl implements ProduitService{

    @Override
    public Response getProduit(int idProduit) {

        Produit produit = null;

        try {
            produit = ProduitsProvider.getProduit(idProduit);
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }

        if(produit.getId() == 0) {
            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.ok(produit).build();
        }

    }

}
