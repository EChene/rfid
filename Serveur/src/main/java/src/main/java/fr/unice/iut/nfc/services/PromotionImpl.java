package src.main.java.fr.unice.iut.nfc.services;

import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnauthorizedWebAppExeption;
import src.main.java.fr.unice.iut.nfc.providers.PromotionsProvider;
import src.main.java.fr.unice.iut.nfc.resources.Promotion;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/promotion")
public class PromotionImpl implements PromotionService{

    @Override
    public Response getPromotion(int idProduit) {

        Promotion promotion = null;

        try {
            promotion = PromotionsProvider.getPromotion(idProduit);
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }

        if(promotion.getId() == 0) {
            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.ok(promotion).build();
        }
    }

}
