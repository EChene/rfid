package src.main.java.fr.unice.iut.nfc.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface ClientService {

    @GET
    @Path("/{idClient}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getClient(@PathParam("idClient") int idClient);

    @GET
    @Path("/liste/")
    @Produces(MediaType.APPLICATION_JSON)
    Response getClients();

}
