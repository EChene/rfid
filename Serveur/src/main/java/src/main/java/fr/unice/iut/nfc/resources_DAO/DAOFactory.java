package src.main.java.fr.unice.iut.nfc.resources_DAO;

import java.sql.Connection;

import static src.main.java.fr.unice.iut.nfc.BDD.ConnectionBDD.getConnection;


public class DAOFactory {

    protected static final Connection conn = getConnection("jdbc:mysql://localhost:3306/nfcpresent?&useSSL=true");

    //protected static final Connection conn = ConnexionSingleton.getConnection();
    /*
     * Méthodes de récupération de l'implémentation des différents DAO
     */
    public static DAO getClientDao() {
        return new ClientDAO( conn );
    }

    public static DAO getAvisDao() {
        return new AvisDAO( conn );
    }

    public static DAO getMagasinDao() {
        return new MagasinDAO( conn );
    }

    public static DAO getProduitDao() { return new ProduitDAO( conn ); }

    public static DAO getPromotionDao() { return new PromotionDAO( conn ); }

}