package src.main.java.fr.unice.iut.nfc.resources_DAO;

import src.main.java.fr.unice.iut.nfc.resources.Produit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ProduitDAO extends DAO<Produit>{

    ProduitDAO(Connection uneConnection) { super(uneConnection); }

    @Override
    public boolean create(Produit unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Produit unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Produit unObjet) throws SQLException {
        return false;
    }

    @Override
    public Produit find(int unId) throws SQLException {
        Produit produit = new Produit();

        String query = "SELECT * FROM PRODUIT WHERE p_id =" + unId;

        ResultSet result = this.connect.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY).executeQuery(query);

        if(result.first()) {
            produit = new Produit(  result.getInt("p_id"),
                                    result.getString("p_nom"),
                                    result.getString("p_descri"),
                                    result.getDouble("p_prix"),
                                    result.getString("p_detail"));
        }

        return produit;
    }

    @Override
    public List<Produit> findAll() throws SQLException {
        return null;
    }

}
