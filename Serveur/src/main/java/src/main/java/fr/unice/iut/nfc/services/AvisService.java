package src.main.java.fr.unice.iut.nfc.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface AvisService {

    @GET
    @Path("/{idProduit}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getAvis(@PathParam("idProduit") int idProduit);

}
