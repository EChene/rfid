package src.main.java.fr.unice.iut.nfc.providers;


import src.main.java.fr.unice.iut.nfc.resources.Magasin;
import src.main.java.fr.unice.iut.nfc.resources_DAO.DAOFactory;
import src.main.java.fr.unice.iut.nfc.resources_DAO.MagasinDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MagasinsProvider {

    private MagasinsProvider() {

    }

    public static List<Magasin> getMagasins(int idProduit) throws SQLException {
        MagasinDAO magasinDAO;
        List<Magasin> magasins = new ArrayList<Magasin>();

        try {
            magasinDAO = (MagasinDAO) DAOFactory.getMagasinDao();
            magasins = magasinDAO.findAll(idProduit);
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return magasins;
    }

    public static Magasin getMagasin(int idMagasin) throws SQLException {
        MagasinDAO magasinDAO;
        Magasin magasin = new Magasin();

        try {
            magasinDAO = (MagasinDAO) DAOFactory.getMagasinDao();
            magasin = magasinDAO.find(idMagasin);
        } catch (SQLException e) {
            throw  new SQLException(e);
        }

        return magasin;
    }

}
