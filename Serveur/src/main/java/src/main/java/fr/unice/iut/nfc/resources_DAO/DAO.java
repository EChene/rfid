package src.main.java.fr.unice.iut.nfc.resources_DAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class DAO<T> {
    protected Connection connect = null;

    public DAO(Connection uneConnection) {
        this.connect = uneConnection;
    }

    /**
     * Méthode de création
     *
     * @param unObjet
     * @return boolean
     */
    public abstract boolean create(T unObjet) throws SQLException;

    /**
     * Méthode pour effacer
     *
     * @param unObjet
     * @return boolean
     */
    public abstract boolean delete(T unObjet) throws SQLException;

    /**
     * Méthode de mise à jour
     *
     * @param unObjet
     * @return boolean
     */
    public abstract boolean update(T unObjet) throws SQLException;

    /**
     * Méthode de recherche des informations
     *
     * @param unId
     * @return T
     */
    public abstract T find(int unId) throws SQLException;

    /**
     * Méthode de recherche des informations
     *
     * @return ArrayList<T>
     */
    public abstract List<T> findAll() throws SQLException;


}