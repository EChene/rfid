package src.main.java.fr.unice.iut.nfc.providers;

import src.main.java.fr.unice.iut.nfc.resources.Avis;
import src.main.java.fr.unice.iut.nfc.resources_DAO.AvisDAO;
import src.main.java.fr.unice.iut.nfc.resources_DAO.DAOFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AvisProvider {

    private AvisProvider() {

    }

    public static List<Avis> getAvis(int idProduit) throws SQLException {
        AvisDAO avisDAO;
        List<Avis> lesAvis = new ArrayList<Avis>();

        try {
            avisDAO = (AvisDAO) DAOFactory.getAvisDao();
            lesAvis = avisDAO.findAll(idProduit);
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return lesAvis;
    }

}
