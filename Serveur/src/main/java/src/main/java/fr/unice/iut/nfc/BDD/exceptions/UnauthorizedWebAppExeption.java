package src.main.java.fr.unice.iut.nfc.BDD.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class UnauthorizedWebAppExeption extends WebApplicationException{


        public UnauthorizedWebAppExeption(){

            super(Response.serverError().build());
        }

        public UnauthorizedWebAppExeption(String reason, Exception cause) {

            super(cause, Response.status(401).entity(reason).build());
        }
}


