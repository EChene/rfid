package src.main.java.fr.unice.iut.nfc.services;

import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnauthorizedWebAppExeption;
import src.main.java.fr.unice.iut.nfc.providers.AvisProvider;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/avis")
public class AvisImpl implements AvisService{

    @Override
    public Response getAvis(int idProduit) {
        try {
            return Response.ok(AvisProvider.getAvis(idProduit)).build();
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }
    }
}
