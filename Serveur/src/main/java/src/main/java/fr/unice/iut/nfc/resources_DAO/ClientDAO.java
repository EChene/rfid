package src.main.java.fr.unice.iut.nfc.resources_DAO;

import src.main.java.fr.unice.iut.nfc.resources.Client;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO extends DAO<Client> {

    public ClientDAO(Connection uneConnection) {
        super(uneConnection);
    }

    @Override
    public boolean create(Client unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Client unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Client unObjet) throws SQLException {
        return false;
    }

    @Override
    public Client find(int unId) throws SQLException {
        Client client = new Client();

        String query = "SELECT * FROM CLIENT WHERE c_id = " + unId;

        ResultSet result = this.connect.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY).executeQuery(query);

        if (result.first()) {
            client = new Client(result.getInt("c_id"),
                                result.getString("c_nom"),
                                result.getString("c_prenom"),
                                result.getBoolean("c_sexe"),
                                result.getString("c_ville"),
                                result.getString("c_adresse"),
                                result.getDate("c_dateNaiss"));
        }

        return client;
    }

    @Override
    public List<Client> findAll() throws SQLException {
        ArrayList<Client> listeClients = new ArrayList<Client>();

        try {
            ResultSet result = this.connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM CLIENT");
            while (result.next())
                listeClients.add(new Client(result.getInt("c_id"),
                                            result.getString("c_nom"),
                                            result.getString("c_prenom"),
                                            result.getBoolean("c_sexe"),
                                            result.getString("c_ville"),
                                            result.getString("c_adresse"),
                                            result.getDate("c_dateNaiss")));

        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return listeClients;
    }

}
