package src.main.java.fr.unice.iut.nfc.providers;

import src.main.java.fr.unice.iut.nfc.resources.Produit;
import src.main.java.fr.unice.iut.nfc.resources_DAO.DAOFactory;
import src.main.java.fr.unice.iut.nfc.resources_DAO.ProduitDAO;

import java.sql.SQLException;

public class ProduitsProvider {

    private ProduitsProvider() {

    }

    public static Produit getProduit(int id) throws SQLException {
        ProduitDAO produitDAO;
        Produit produit = new Produit();

        try {
            produitDAO = (ProduitDAO) DAOFactory.getProduitDao();
            produit = produitDAO.find(id);
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return produit;
    }

}
