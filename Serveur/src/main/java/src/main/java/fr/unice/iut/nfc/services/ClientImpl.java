package src.main.java.fr.unice.iut.nfc.services;

import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnauthorizedWebAppExeption;
import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnknownWebAppException;
import src.main.java.fr.unice.iut.nfc.providers.ClientsProvider;
import src.main.java.fr.unice.iut.nfc.resources.Client;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/client")
public class ClientImpl implements ClientService {

    @Override
    public Response getClient(int idClient) {

        Client client = null;

        try {
            client = ClientsProvider.getClient(idClient);
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }

        if(client.getId() == 0) {
            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return  Response.ok(client).build();
        }
    }

    @Override
    public Response getClients() {
        try {
            return Response.ok(ClientsProvider.getClients()).build();
        } catch (Exception e) {
            throw new UnknownWebAppException("Exeption",e);
        }
    }
}
