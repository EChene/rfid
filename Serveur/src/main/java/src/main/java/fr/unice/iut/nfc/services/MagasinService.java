package src.main.java.fr.unice.iut.nfc.services;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface MagasinService {

    @GET
    @Path("/produit/{idProduit}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getMagasins(@PathParam("idProduit") int idProduit);

    @GET
    @Path("/{idMagasin}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getMagasin(@PathParam("idMagasin") int idMagasin);

}
