package src.main.java.fr.unice.iut.nfc.BDD.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class UnknownWebAppException extends WebApplicationException {

    public UnknownWebAppException(){
        super(Response.serverError().build());
    }
    public UnknownWebAppException(String reason, Exception cause) {
        super(cause, Response.status(500).entity(reason).build());
    }
}
