package src.main.java.fr.unice.iut.nfc.providers;

import src.main.java.fr.unice.iut.nfc.resources.Client;
import src.main.java.fr.unice.iut.nfc.resources_DAO.ClientDAO;
import src.main.java.fr.unice.iut.nfc.resources_DAO.DAOFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ClientsProvider {

    private ClientsProvider() {

    }

    public static List<Client> getClients() throws  SQLException {
        ClientDAO clientDAO;
        List<Client> clients = new ArrayList<Client>();

        try {
            clientDAO = (ClientDAO) DAOFactory.getClientDao();
            clients = clientDAO.findAll();
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return clients;
    }

    public static Client getClient(int id) throws SQLException {
        ClientDAO clientDAO;
        Client client = new Client();

        try {
            clientDAO = (ClientDAO) DAOFactory.getClientDao();
            client = clientDAO.find(id);
        } catch (SQLException e) {
            throw  new SQLException(e);
        }

        return client;
    }
}
