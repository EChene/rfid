package src.main.java.fr.unice.iut.nfc.services;

import src.main.java.fr.unice.iut.nfc.BDD.exceptions.UnauthorizedWebAppExeption;
import src.main.java.fr.unice.iut.nfc.providers.MagasinsProvider;
import src.main.java.fr.unice.iut.nfc.resources.Magasin;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/magasin")
public class MagasinImpl implements  MagasinService{

    @Override
    public Response getMagasin(int idMagasin) {

        Magasin magasin = null;

        try {
            magasin = MagasinsProvider.getMagasin(idMagasin);
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }

        if(magasin.getId() == 0) {
            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.ok(magasin).build();
        }
    }

    @Override
    public Response getMagasins(int idProduit) {
        try {
            return Response.ok(MagasinsProvider.getMagasins(idProduit)).build();
        } catch (Exception e) {
            throw new UnauthorizedWebAppExeption("Exception", e);
        }
    }
}
