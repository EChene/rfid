package src.main.java.fr.unice.iut.nfc.BDD.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class BadRequestWebAppException extends WebApplicationException{

    public BadRequestWebAppException(){
        super(Response.serverError().build());
    }
    public BadRequestWebAppException(String reason, Exception cause) {
        super(cause, Response.status(400).entity(reason).build());
    }
}
