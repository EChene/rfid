package src.main.java.fr.unice.iut.nfc.resources_DAO;

import src.main.java.fr.unice.iut.nfc.resources.Magasin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MagasinDAO extends DAO<Magasin>{

    MagasinDAO(Connection uneConnection) { super(uneConnection); }

    @Override
    public boolean create(Magasin unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Magasin unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Magasin unObjet) throws SQLException {
        return false;
    }

    @Override
    public Magasin find(int unId) throws SQLException {
        Magasin magasin = new Magasin();

        String query = "SELECT * FROM MAGASIN WHERE m_id = " + unId;

        ResultSet result = this.connect.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY).executeQuery(query);

        if(result.first()) {
            magasin = new Magasin(  result.getInt("m_id"),
                                    result.getString("m_nom"),
                                    result.getString("m_adresse"));
        }

        return magasin;
    }

    @Override
    public List<Magasin> findAll() throws SQLException {
        return null;
    }

    public List<Magasin> findAll(int idProduit) throws SQLException {
        ArrayList<Magasin> magasins = new ArrayList<Magasin>();

        try {
            ResultSet result = this.connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT magasin.m_id, magasin.m_nom, magasin.m_adresse, situer.quantite FROM MAGASIN, SITUER WHERE magasin.m_id = situer.m_id AND magasin.m_id IN (SELECT m_id FROM SITUER WHERE p_id = " + idProduit + " GROUP BY m_id) GROUP BY magasin.m_id");
            while (result.next())
                magasins.add(new Magasin(   result.getInt("m_id"),
                                            result.getString("m_nom"),
                                            result.getString("m_adresse"),
                                            result.getInt("quantite")));

        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return magasins;
    }

}
