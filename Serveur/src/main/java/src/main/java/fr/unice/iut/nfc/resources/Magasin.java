package src.main.java.fr.unice.iut.nfc.resources;

public class Magasin {

    private int id;
    private String nom;
    private String adresse;
    private int quantite;

    public Magasin(int unId, String unNom, String uneAdresse){

        this.id = unId;
        this.nom = unNom;
        this.adresse = uneAdresse;
        this.quantite = 0;

    }

    public Magasin(int unId, String unNom, String uneAdresse, int uneQuantite){

        this.id = unId;
        this.nom = unNom;
        this.adresse = uneAdresse;
        this.quantite = uneQuantite;

    }

    public Magasin() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getQuantite() { return quantite; }

    public void setQuantite(int quantite) { this.quantite = quantite; }


}
