package src.main.java.fr.unice.iut.nfc.providers;

import src.main.java.fr.unice.iut.nfc.resources.Promotion;
import src.main.java.fr.unice.iut.nfc.resources_DAO.DAOFactory;
import src.main.java.fr.unice.iut.nfc.resources_DAO.PromotionDAO;

import java.sql.SQLException;

public class PromotionsProvider {

    private PromotionsProvider() {

    }

    public static Promotion getPromotion(int id) throws SQLException {
        PromotionDAO promotionDAO;
        Promotion promotion = new Promotion();

        try {
            promotionDAO = (PromotionDAO) DAOFactory.getPromotionDao();
            promotion = promotionDAO.find(id);
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return promotion;
    }

}
