package src.main.java.fr.unice.iut.nfc.resources_DAO;

import src.main.java.fr.unice.iut.nfc.resources.Avis;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AvisDAO extends DAO<Avis>{

    public AvisDAO(Connection uneConnection) { super(uneConnection); }

    @Override
    public boolean create(Avis unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Avis unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Avis unObjet) throws SQLException {
        return false;
    }

    @Override
    public Avis find(int unId) throws SQLException {
        return null;
    }

    @Override
    public List<Avis> findAll() throws SQLException {
        return null;
    }

    public List<Avis> findAll(int idProduit) throws SQLException {
        ArrayList<Avis> listeAvis = new ArrayList<Avis>();

        try {
            ResultSet result = this.connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM AVIS WHERE a_p_id = " + idProduit);
            while (result.next())
                listeAvis.add(new Avis( result.getInt("a_p_id"),
                                        result.getInt("a_c_id"),
                                        result.getInt("a_note"),
                                        result.getString("a_descri")));


        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return listeAvis;
    }
}
