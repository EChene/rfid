package src.main.java.fr.unice.iut.nfc.resources_DAO;

import src.main.java.fr.unice.iut.nfc.resources.Promotion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PromotionDAO extends DAO<Promotion> {

    PromotionDAO(Connection uneConnection) { super(uneConnection); }

    @Override
    public boolean create(Promotion unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Promotion unObjet) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Promotion unObjet) throws SQLException {
        return false;
    }

    @Override
    public Promotion find(int unId) throws SQLException {
        Promotion promotion = new Promotion();

        String query = "SELECT promotion.p_id, promotion.p_reduction, promotion.p_descri, profite.pr_debut, profite.pr_fin FROM promotion, profite WHERE profite.pr_p_id = promotion.p_id AND profite.pr_p_id = " + unId + " GROUP BY promotion.p_id";

        ResultSet result = this.connect.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY).executeQuery(query);

        if(result.first()) {
            promotion = new Promotion(  result.getInt("p_id"),
                                        result.getInt("p_reduction"),
                                        result.getString("p_descri"),
                                        result.getDate("pr_debut"),
                                        result.getDate("pr_fin"));
        }

        return promotion;
    }

    @Override
    public List<Promotion> findAll() throws SQLException {
        return null;
    }

}
