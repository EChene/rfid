package com.unice.iut.NFC.ressources;

import java.util.Date;

public class Client {

    private int     id;
    private String nom;
    private String prenom;
    private boolean sexe;
    private String ville;
    private String adresse;
    private Date dateNaiss;

    public Client(int unId, String unNom, String unPrenom, boolean unSexe, String uneVille, String uneAdresse, Date uneDateNaiss) {

        this.id         = unId;
        this.nom        = unNom;
        this.prenom     = unPrenom;
        this.sexe       = unSexe;
        this.ville      = uneVille;
        this.adresse    = uneAdresse;
        this.dateNaiss  = uneDateNaiss;

    }

    public Client() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public boolean isSexe() {
        return sexe;
    }

    public void setSexe(boolean sexe) {
        this.sexe = sexe;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

}
