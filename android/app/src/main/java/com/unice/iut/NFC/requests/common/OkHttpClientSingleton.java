package com.unice.iut.NFC.requests.common;

import okhttp3.OkHttpClient;

public class OkHttpClientSingleton {

    private OkHttpClientSingleton() {
    }

    private static class OkHttpClientSingletonHolder {
        private static OkHttpClient INSTANCE = new OkHttpClient();
    }

    public static OkHttpClient getInstance() {
        return OkHttpClientSingletonHolder.INSTANCE;
    }
}
