package com.unice.iut.NFC.ressources;

public class Avis {

    private int     idProduit;
    private int     idClient;
    private int     note;
    private String description;

    public Avis(int unIdProduit, int unIdClient, int uneNote, String uneDescription) {

        this.idProduit      = unIdProduit;
        this.idClient       = unIdClient;
        this.note           = uneNote;
        this.description    = uneDescription;

    }

    public Avis() {}

    public int getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
