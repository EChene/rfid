package com.unice.iut.NFC.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unice.iut.NFC.R;
import com.unice.iut.NFC.ressources.Promotion;

import java.util.List;

public class PromotionAdapter extends ArrayAdapter<Promotion> {
    private final Context context;
    private final List<Promotion> data;

    public PromotionAdapter(Context context, List<Promotion> data) {
        super(context, 0, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int idProduit, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.produit, parent, false);
        }

        PromotionAdapter.ViewHolder promotionViewHolder = (PromotionAdapter.ViewHolder) convertView.getTag();

        if(promotionViewHolder == null)
        {
            promotionViewHolder = new PromotionAdapter.ViewHolder();
            promotionViewHolder.tv_pourcentage = (TextView) convertView.findViewById(R.id.tv_pourcentage);
            promotionViewHolder.tv_dateDebut = (TextView) convertView.findViewById(R.id.tv_dateDebut);
            promotionViewHolder.tv_dateFin = (TextView) convertView.findViewById(R.id.tv_dateFin);

            convertView.setTag(promotionViewHolder);
        }

        Promotion promotion = getItem(idProduit);
        promotionViewHolder.tv_pourcentage.setText(promotion.getPourcentage());
        promotionViewHolder.tv_dateDebut.setText(promotion.getDateDebut() + "");
        promotionViewHolder.tv_dateFin.setText(promotion.getDateFin() + "");

        convertView.setSelected(true);

        return convertView;

    }

    public class ViewHolder
    {
        TextView tv_pourcentage;
        TextView tv_dateDebut;
        TextView tv_dateFin;
    }
}