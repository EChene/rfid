package com.unice.iut.NFC.requests;

import android.util.Log;

import com.unice.iut.NFC.requests.common.RootRequest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;

import okhttp3.Callback;
import okhttp3.Request;

public class MagasinRequest extends RootRequest {

    private int idProduit;

    public MagasinRequest(int idProduit) throws MalformedURLException {
        this.idProduit = idProduit;
    }

    public void connect(Callback callback) throws NoSuchAlgorithmException, IOException, InterruptedException {

        String url = JETTYSERVER + WAR + "magasin/produit/" + idProduit;

        Log.i("URL", url);

        Request request = new Request.Builder()
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(callback);
    }
}
