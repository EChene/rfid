package com.unice.iut.NFC.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unice.iut.NFC.R;
import com.unice.iut.NFC.ressources.Produit;

import java.util.List;


public class ProduitAdapter extends ArrayAdapter<Produit> {
    private final Context context;
    private final List<Produit> data;

    public ProduitAdapter(Context context, List<Produit> data) {
        super(context, 0, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int idProduit, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.produit, parent, false);
        }

        ViewHolder produitViewHolder = (ViewHolder) convertView.getTag();

        if(produitViewHolder == null)
        {
            produitViewHolder = new ViewHolder();
            produitViewHolder.tv_nom = (TextView) convertView.findViewById(R.id.tv_nom);
            produitViewHolder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
            produitViewHolder.tv_detail = (TextView) convertView.findViewById(R.id.tv_detail);
            produitViewHolder.tv_prix = (TextView) convertView.findViewById(R.id.tv_prix);

            convertView.setTag(produitViewHolder);
        }

        Produit produit = getItem(idProduit);
        produitViewHolder.tv_nom.setText(produit.getNom());
        produitViewHolder.tv_description.setText(produit.getDescription());
        produitViewHolder.tv_detail.setText(produit.getDetail());
        produitViewHolder.tv_prix.setText(produit.getPrix() + "");

        convertView.setSelected(true);

        return convertView;

    }

    public class ViewHolder
    {
        TextView tv_nom;
        TextView tv_description;
        TextView tv_detail;
        TextView tv_prix;
    }
}
