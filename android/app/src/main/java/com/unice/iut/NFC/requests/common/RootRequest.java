package com.unice.iut.NFC.requests.common;

import okhttp3.OkHttpClient;

public class RootRequest {

    protected static final String JETTYSERVER = "http://172.20.10.5:8080/";
    protected static final String WAR = "nfc/";

    protected OkHttpClient okHttpClient = OkHttpClientSingleton.getInstance();
}
