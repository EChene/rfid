package com.unice.iut.NFC.activities;


import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.unice.iut.NFC.R;
import com.unice.iut.NFC.requests.MagasinRequest;
import com.unice.iut.NFC.ressources.Magasin;
import com.unice.iut.NFC.ressources.Produit;
import com.unice.iut.NFC.ressources.Promotion;
import com.unice.iut.NFC.requests.ProduitRequest;
import com.unice.iut.NFC.requests.PromotionRequest;
import com.unice.iut.NFC.view.adapters.ProduitAdapter;
import com.unice.iut.NFC.view.adapters.PromotionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class ProduitActivity extends ListActivity{


    static private ProduitAdapter.ViewHolder produitViewHolder;

    private ListView mListView;

    private int idProduit;

    private TextView mTvNom;
    private TextView mTvDetail;
    private TextView mTvPrix;
    private TextView mTvDescription;

    List<Produit> produits = new ArrayList<Produit>();
    List<Promotion> promotions = new ArrayList<Promotion>();
    List<Magasin> magasins = new ArrayList<Magasin>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produit);


        String value = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            value = bundle.getString("ID_PRODUIT");
        }

        idProduit = Integer.parseInt(value.toString());

        mListView = (ListView) findViewById(R.id.lv_produits);

        mTvNom = (TextView) findViewById(R.id.tv_nom);
        mTvDetail = (TextView) findViewById(R.id.tv_detail);
        mTvDescription = (TextView) findViewById(R.id.tv_description);
        mTvPrix = (TextView) findViewById(R.id.tv_prix);


        getProduits(idProduit);
        getPromotions(idProduit);
        getMagasins(idProduit);


    }

    private void getProduits(int idProduit) {
        try {

            Log.e("ID PRODUIT", idProduit + "");

            ProduitRequest produitRequest = new ProduitRequest(idProduit);

            produitRequest.connect(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Erreur : Les produits n'ont pas pu être chargés",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String produit = response.body().string();

                    Log.i("JSON produit", produit + "");

                    try {

                        JSONObject json = new JSONObject(produit);

                            Log.e("ID produit récupéré", json.getInt("id") + "");

                            produits.add(new Produit(json.getInt("id"), json.getString("nom"), json.getString("description"), json.getDouble("prix"), json.getString("detail")));
                        //}

                        setListViewProduit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (InterruptedException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void getMagasins(int idProduit) {
        try {

            MagasinRequest magasinRequest = new MagasinRequest(idProduit);

            magasinRequest.connect(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Erreur : Les magasins n'ont pas pu être chargés",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String magasin = response.body().string();

                    Log.i("JSON magasin", magasin + "");

                    try {

                        JSONArray jsonArray = new JSONArray(magasin);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject json = new JSONObject(jsonArray.getString(i));
                            magasins.add(new Magasin(json.getInt("id"), json.getString("nom"), json.getString("adresse"), json.getInt("quantite")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (InterruptedException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void getPromotions(int idProduit) {
        try {

            PromotionRequest promotionRequest = new PromotionRequest(idProduit);

            promotionRequest.connect(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Erreur : Les promotions n'ont pas pu être chargées",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String promotion = response.body().string();



                    Log.i("JSON promotion", promotion + "");

                    try {

                        JSONObject json = new JSONObject(promotion);

                        String sDateDebut = json.getString("dateDebut");
                        String sDateFin = json.getString("dateDebut");

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateDebut = sdf.parse(sDateDebut);
                        Date dateFin = sdf.parse(sDateFin);

                        promotions.add(new Promotion(json.getInt("id"), json.getInt("pourcentage"), json.getString("description"), dateDebut, dateFin));

                        //setListViewPromotion();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (InterruptedException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void setListViewProduit() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProduitAdapter produitAdapter = new ProduitAdapter(ProduitActivity.this, produits);
                mListView.setAdapter(produitAdapter);

            }
        });
    }

    private void setListViewPromotion() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PromotionAdapter promotionAdapter = new PromotionAdapter(ProduitActivity.this, promotions);
                mListView.setAdapter(promotionAdapter);

            }
        });
    }

}
