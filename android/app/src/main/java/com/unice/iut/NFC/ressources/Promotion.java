package com.unice.iut.NFC.ressources;

import java.util.Date;

public class Promotion {

    private int     id;
    private int     pourcentage;
    private String description;
    private Date dateDebut;
    private Date dateFin;

    public Promotion(int unId, int unPourcentage, String uneDescription, Date uneDateDebut, Date uneDateFin) {

        this.id             = unId ;
        this.pourcentage    = unPourcentage;
        this.description    = uneDescription;
        this.dateDebut      = uneDateDebut;
        this.dateFin        = uneDateFin;

    }

    public Promotion() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(int pourcentage) {
        this.pourcentage = pourcentage;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
